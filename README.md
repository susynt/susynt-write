susynt-write
============

# Contents

* [Introduction](#introduction)
* [Requirements](#requirements)
* [Actions](#actions)
  * [First Time Setup](#first-time-setup)
  * [Subsequent Area Setup](#subsequent-area-setup)
  * [Subsequent Compilation](#subsequent-compilation)
  * [Compiling after Changes to CMakeLists](#compiling-after-changes-to-cmakelists)


## Introduction
This package contains several scripts that can be used to setup an area to run the necessary software to process DAOD files to produce susyNt files. It checks out **SusyNtuple**, **SusyCommon**, and **SUSYTools**, as well as any other packages required (typically those specified in *SUSYTools/doc/packages.txt*).

## Requirements
The assumption is that the checked out software will be run on a machine with access to **cvmfs**. We need **cvmfs** in order to check out a specific **AnalysisBase** release to gain access to all of the core xAOD and ASG tools. You need:

1) access to **cvmfs** (be on a machine with cvmfs)
2) setup ATLAS software environment (run: ```source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh```)
3) kerberos tickets (run: ```kinit -f ${USER}@CERN.CH```)

## Actions

### Setting up your *athena* cache

The software that is setup by the *susynt-write* scripts relies on [atlas/athena](https://gitlab.cern.ch/atlas/athena). This means that that
in order for it to work we need to **sparse checkout** the [atlas/athena](https://gitlab.cern.ch/atlas/athena) repository. If you
are unfamiliar with what this means please go directly to the
[ATLAS Git Workflow Tutorial](https://atlassoftwaredocs.web.cern.ch/gittutorial/)
before moving ahead.

Performing a fresh sparse-checkout can be a time consuming operation given the abusive
nature [atlas/athena's](https://gitlab.cern.ch/atlas/athena) use of *git*. Rather than do
this it is recommended that you have a "git cache" that you checkout in a common, safe
area and then bounce any subsequent sparse checkouts *off of that already cloned repository*.
This is described in the [LearningGit Twiki](https://twiki.cern.ch/twiki/bin/view/Main/LearningGit#2_Creating_your_cache_repository).

The installation of the software will go much faster if you have such a "git cache" installed. By default,
the SusyNtuple-related software will expect this cache to be located in
`${HOME}/susynt_gitcache/athena`. You can obtain such a cache by doing the following steps:

```bash
cd ${HOME}
mkdir susynt_gitcache
lsetup git
git atlas init-workdir https://:@gitlab.cern.ch:8443/susynt/athena.git
```

Using a git cache of the athena repository will *significantly* speed up any subsequent work dir initialization.



### First Time Setup of *susynt-write*
Here are the steps to setup an area from scratch.

```bash
git clone -b <tag> https://:@gitlab.cern.ch:8443/susynt/susynt-write.git
cd susynt-write/
source bash/setup_area.sh
source bash/setup_release.sh --compile
```

**Note**: by default the script assumes that you both wish to use a git cache (as described above)
and that that cache is located in the default location of `${HOME}/susynt_gitcache/athena`. If you
wish to use another gitcache, you can provide its location by adding to the above third line:

```bash
source bash/setup_area.sh --stable --gitcache <path-to-cache>
```

If you wish to skip the use of a git cache altogether and do the full sparse checkout of 
[atlas/athena](https://gitlab.cern.ch/atlas/athena) instead, do:

```bash
source bash/setup_area.sh --stable --no-cache
```

The script *bash/setup_area.sh* call in the above code snippet will checkout the "stable" release given by the tag ```<tag>```.  This means that it will checkout the associated tags of **SusyNtuple** and **SusyCommon** (i.e. those tags of these packages that were used to build susyNt tag ```<tag>```). You can use the ```-h``` or ```--help``` option to see the full list of options:

```
source bash/setup_area.sh --help
```

You will see that you can give the ```--sc``` or ```--sn``` option to specify specific tags of **SusyCommon** or **SusyNtuple**, respectively.

The script *bash/setup_release.sh* sets up the associated **AnalysisBase** release. When given the ```--compile``` flag it will also run the full compilation of the packages checked out by the *bash/setup_area.sh* script (the packages which should now live under the *susynt-write/source/* directory). You can use the ```-h``` or ```--help``` option to see the full list of options:

```
source bash/setup_release.sh --help
```

After running *bash/setup_release.sh* with the ```--compile``` flag you will see the *susynt-write/build/* directory which contains the typical ```CMake```-like build directory structure. In order to allow all of the executables be in the user's path, the *bash/setup_release.sh* script sources the *setup.sh* script located in *susynt-write/build/x86_64-\*/* directory.

### Subsequent Area Setup
If you are returning to your *susynt-write* directory from a new shell and you have previously compiled all of the software, you need to still setup the environment so that all of the executables, librarires, etc... can be found. You can do this simpy by calling the *bash/setup_release.sh* script with no arguments:

```
source bash/setup_release.sh
```

This sources the *setup.sh* script located in *susynt-write/build/x86_64-\*/* directory. 

### Subsequent Compilation
You can either call:

```
source bash/setup_release.sh --compile
```

every time you wish to compile. But this runs the *cmake* command to initiate the ```CMake``` configuration steps. This also removes the previous *build/* directory and starts a new one.

The simpler and faster way (and therefore recommended way) is to move to the *build/* directory and simply call ```make```:

```
cd build/
make
```

### Compiling after Changes to CMakeLists

If you change any of the ```CMakeLists.txt``` files in any of the packages in *susynt/source/* directory, you need to re-run the ```CMake``` configuration. You can do this simply by running:

```
source bash/setup_release.sh --compile
```

or, if you do not want to completely remove the previous *build/* directory (and are sure that your changes are OK for this) you can simply do:

```
cd build/
cmake ../source
make
```
