#!/bin/bash

######################################################################
# setup_area
#
# script to checkout the packages for a given release
# of susyNt and to setup the area to run the software
#
# daniel.joseph.antrim@cern.ch
# August 2017
#
######################################################################

default_gitcache="${HOME}/susynt_gitcache/athena"
default_release="AnalysisBase,21.2.60,here"
default_athena_tag="release/21.2.60"
stable_sn_tag="SusyNtuple-00-08-03" # n0307
stable_sc_tag="SusyCommon-00-07-02" # n0307

function print_usage {
    echo "------------------------------------------------"
    echo "setup_area"
    echo ""
    echo "Options:"
    echo " --gitcache           Cache location to use for athena [default:${default_gitcache}]"
    echo " --no-cache           Do not use a git cache, do full sparse checkout instead [default:false]"
    echo " --master             Checkout master branches of SusyCommon and SusyNtuple"
    echo " --sn                 Set branch/tag to checkout for SusyNtuple"
    echo " --sc                 Set branch/tag to checkout for SusyCommon"
#    echo " --skip-patch         Do not perform patch of SUSYTools"
    echo " -h|--help            Print this help message"
    echo ""
    echo "Example usage:"
    echo " - Setup the area for stable use:"
    echo "   $ source setup_area.sh"
    echo " - Checkout 'cmake' and 'r21' branches of SusyNtuple and SusyCommon:"
    echo "   $ source setup_area.sh --sn cmake --sc r21" 
    echo " NB A tag/branch for SusyCommon AND SusyNtuple is required"
    echo "------------------------------------------------"
}

function prepare_directories {

    # make sources directory for dumping the code
    dirname="source/"
    if [[ -d "$dirname" ]]; then
        echo "$dirname directory exists"
    else
        echo "setup_area    Creating $dirname directory"
    fi
    mkdir -p $dirname

    #cp patchSUSYTools.patch $dirname 
}

function get_externals_git {

    skip_patch=$1
    gitcache_location=$2
    dont_use_cache=$3

    # setup atlas git
    startdir=${PWD}
    sourcedir="./source/"
    if [[ -d $sourcedir ]]; then
        cd $sourcedir
    else
        echo "setup_area    ERROR No $sourcedir directory"
        return 1
    fi

    sourcedir=${PWD}

    echo "setup_area    Setting up the AnalysisBase release"
    nightly_string=$(date +%Y:%m:%d -d "-6 day" | sed 's/:/-/g')
#    echo "setup_area    INFO Setting up nightly release from : ${nightly_string}"
    #lsetup "asetup 21.2,AnalysisBase,r${nightly_string},here"
    #lsetup "asetup 21.2,AnalysisBase,latest,here"
    echo "setup_area    Setting up release: ${default_release}"
    lsetup "asetup ${default_release}"

    lsetup git
    if [[ $dont_use_cache == 0 ]]; then
        echo "setup_area    Calling: 'git atlas init-workdir $gitcache_location'"
        git atlas init-workdir $gitcache_location
    else
        echo "setup_area    Calling: 'git atlas init-workdir https://:@gitlab.cern.ch:8443/susynt/athena.git'"
        git atlas init-workdir https://:@gitlab.cern.ch:8443/susynt/athena.git
    fi

    if [[ -d "./athena/" ]]; then

        cd $startdir
        cp patchSUSYTools.patch $sourcedir/athena/
        cd $sourcedir/athena/

        #echo "setup_area    Checking out ATLAS SW release ${nightly_string}"
        echo "setup_area    Checking out athena tag: ${default_athena_tag}"
        git fetch upstream

        #nightly_tag=$(basename $ATLAS_RELEASE_BASE)
        #git checkout nightly/21.2/${nightly_tag} #${nightly_string}
        git checkout $default_athena_tag
        git atlas addpkg SUSYTools

        if [[ $skip_patch == 0 ]]; then
            if [[ -f "patchSUSYTools.patch" ]]; then
                echo "Patching SUSYTools"
                git apply patchSUSYTools.patch
            else
                echo "Patch file 'patchSUSYTools.patch' not found, cannot patch SUSYTools!"
            fi
        fi
    else
        echo "setup_area    ERROR Did not get ATLAS SW repository (have you forked it yet?)"
        return 1
    fi

    cd $sourcedir

    rmdir="./athena/Projects/"
    if [[ -d $rmdir ]]; then
        cmd="rm -r $rmdir"
        $cmd
    fi

    cd $startdir

}


function get_susynt {

    sc_tag=$1
    sn_tag=$2

    dirname="./source/"
    startdir=${PWD}
    if [[ -d $dirname ]]; then
        cd $dirname
    else
        echo "ERROR get_susynt $dirname directory not found"
        return 1
    fi

    git clone -b master https://:@gitlab.cern.ch:8443/susynt/SusyNtuple.git
    git clone -b master https://:@gitlab.cern.ch:8443/susynt/SusyCommon.git

    cd ./SusyNtuple/
    git checkout $sn_tag
    cd -
    cd ./SusyCommon/
    git checkout $sc_tag
    cd -

    cd ${startdir}
}

function main {

    sn_tag=$stable_sn_tag
    sc_tag=$stable_sc_tag
    skip_patch=1
    gitcache=$default_gitcache
    no_cache=0

    while test $# -gt 0
    do
        case $1 in
            --master)
                sn_tag="master"
                sc_tag="master"
                ;;
            --gitcache)
                gitcache=${2}
                shift
                ;;
            --no-cache)
                no_cache=1
                ;;
            --sn)
                sn_tag=${2}
                shift
                ;;
            --sc)
                sc_tag=${2}
                shift
                ;;
#            --skip-patch)
#                skip_patch=1
#                ;;
            -h)
                print_usage
                return 0
                ;;
            --help)
                print_usage
                return 0
                ;;
            *)
                echo "ERROR Invalid argument: $1"
                return 1
                ;;
        esac
        shift
    done

    if [[ $sn_tag == "" ]]; then
        echo "ERROR SusyNtuple tag is not set"
        return 1
    fi
    if [[ $sc_tag == "" ]]; then
        echo "ERROR SusyCommon tag is not set"
        return 1
    fi

    echo "setup_area    Starting -- `date`"

    echo "setup_area    Using gitcache location     :   $gitcache"
    echo "setup_area    Checking out SusyNtuple tag :   $sn_tag"
    echo "setup_area    Checking out SusyCommon tag :   $sc_tag"

    prepare_directories
    get_externals_git $skip_patch $gitcache $no_cache
    get_susynt $sc_tag $sn_tag

    echo "setup_area    Finished -- `date`"
}

main $*
